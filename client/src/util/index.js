export const interpolate = (prev, next, difference, fromPrev) => {
  return prev + ((next - prev) / difference) * fromPrev;
};

export const get32BitTimestamp = timestamp => {
  const now = timestamp || Date.now();
  const buff = Buffer.from(now.toString(16).padStart(12, "0"), 'hex');
  return buff.readUInt32BE(2);
}

export const calculateFrameData = (prev, next, delta, DELAY) => {
  const now = get32BitTimestamp() - DELAY - delta;
  if (prev.timestamp && next.timestamp && delta) {
    const diff = next.timestamp - prev.timestamp;
    const fromPrev = now - prev.timestamp;

    return {
      players: prev.players.map(prevPlayerState => {
        const nextPlayerState = next.players.find(
          nextPlayer => nextPlayer.id === prevPlayerState.id
        );
        return {
          ...prevPlayerState,
          position: {
            x: nextPlayerState
              ? interpolate(
                prevPlayerState.position.x,
                nextPlayerState.position.x,
                diff,
                fromPrev
              )
              : prevPlayerState.position.x,
            y: nextPlayerState
              ? interpolate(
                prevPlayerState.position.y,
                nextPlayerState.position.y,
                diff,
                fromPrev
              )
              : prevPlayerState.position.y
          }
        };
      }),
      food: prev.food.map(prevFoodState => {
        const nextFoodState = next.food.find(
          nextFood => nextFood.id === prevFoodState.id
        );
        return {
          ...prevFoodState,
          position: {
            x: nextFoodState
              ? interpolate(
                prevFoodState.position.x,
                nextFoodState.position.x,
                diff,
                fromPrev
              )
              : prevFoodState.position.x,
            y: nextFoodState
              ? interpolate(
                prevFoodState.position.y,
                nextFoodState.position.y,
                diff,
                fromPrev
              )
              : prevFoodState.position.y
          }
        };
      }),
      center: {
        x: interpolate(prev.center.x, next.center.x, diff, fromPrev),
        y: interpolate(prev.center.y, next.center.y, diff, fromPrev)
      }
    };
  }
};
