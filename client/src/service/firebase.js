import * as firebase from "firebase/app";
import "firebase/auth";
import "firebase/database";

const config = {
    apiKey: "AIzaSyA7wFTMdsnAlNISM1N-dfrsrEAgn_Zsm-I",
    authDomain: "io-engine-a6fb0.firebaseapp.com",
    databaseURL: "https://io-engine-a6fb0.firebaseio.com",
    projectId: "io-engine-a6fb0",
    storageBucket: "",
    messagingSenderId: "414889222309",
    appId: "1:414889222309:web:6f8024ada02c4d38"
  };
  
  firebase.initializeApp(config);
  
  export const auth = firebase.auth();
  export const db = firebase.database();

  export default firebase;
