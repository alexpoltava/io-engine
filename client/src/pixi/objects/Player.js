import * as PIXI from "pixi.js";
import circleTexture from "../../images/circle.svg";
import {
    CIRCLE_SVG_SIZE,
} from "../../config";

const playerSprite = PIXI.Sprite.from(circleTexture);

class Player {
    constructor({ name, x, y, radius, color = 0xf0f0f0 }) {
        this.container = new PIXI.Container();
        this.container.x = x;
        this.container.y = y;

        playerSprite.anchor.set(0.5);

        playerSprite.scale = {
            x: radius * 2 / CIRCLE_SVG_SIZE,
            y: radius * 2 / CIRCLE_SVG_SIZE
        };
        playerSprite.tint = color;

        this.container.addChild(playerSprite);

        const playerName = new PIXI.Text(name, { fontFamily: 'Arial', fontSize: Math.round(16 * radius / 20), align: 'center', fill: 0xFFFFFF, stroke: 0x000000, strokeThickness: Math.round(3 * radius / 20) });
        playerName.anchor.set(0.5);

        this.container.addChild(playerName);
    }

    render() {
        return this.container;
    }
}

export default Player;