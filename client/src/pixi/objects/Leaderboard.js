import * as PIXI from "pixi.js";
import leaderboardTexture from "../../images/leaderboard.svg";

const MARGIN = 10;
const PADDING = 10;

class Leaderboard {
  constructor({ x, y }) {
    this.container = new PIXI.Container();

    this.leaderboardBackground = PIXI.Sprite.from(leaderboardTexture);
    this.leaderboardBackground.tint = 0x000000;
    this.leaderboardBackground.alpha = 0.5;
    this.container.addChild(this.leaderboardBackground);

    this.leaderboardHeader = new PIXI.Text("Leaderboard", {
      fontFamily: "Arial",
      fontWeight: "bold",
      fontSize: 24,
      align: "center",
      fill: 0xf0f0f0,
    });
    this.leaderboardHeader.x = (200 - this.leaderboardHeader.width) / 2;
    this.container.addChild(this.leaderboardHeader);

    this.leaderboardList = new PIXI.Container();
    this.container.addChild(this.leaderboardList);

    this.leaderboardBackground.scale.set(
      1,
      (this.leaderboardHeader.height + PADDING) / 300
    );
    this.update({ x, y });
  }

  update({ x, y, statistics = [] }) {
    this.container.x = x - this.leaderboardBackground.width - MARGIN;
    this.container.y = y + MARGIN;

    this.leaderboardList.removeChildren();
    for (const record of statistics) {
      const leader = new PIXI.Text(
        `${record.place}. ${record.name} ${record.mass / 10}`,
        {
          fontFamily: "Arial",
          fontWeight: "bold",
          fontSize: 14,
          align: "left",
          fill: record.me ? 0xf08080 : 0xf0f0f0,
        }
      );
      leader.x = 10;
      leader.y = this.leaderboardHeader.height + leader.height * record.place;
      this.leaderboardList.addChild(leader);
      this.leaderboardBackground.scale.set(
        1,
        (this.leaderboardHeader.height +
          leader.height * (record.place + 1) +
          PADDING) /
          300
      );
    }
  }

  render() {
    return this.container;
  }
}

export default Leaderboard;
