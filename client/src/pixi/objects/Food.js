import * as PIXI from "pixi.js";
import circleTexture from "../../images/circle.svg";
import {
    CIRCLE_SVG_SIZE,
} from "../../config";

class Food {
    constructor({ x, y, radius, color = 0xf0f0f0 }) {
        this.foodSprite = PIXI.Sprite.from(circleTexture);
        this.foodSprite.x = x;
        this.foodSprite.y = y;

        this.foodSprite.anchor.set(0.5);

        this.foodSprite.scale = {
            x: radius * 2 / CIRCLE_SVG_SIZE,
            y: radius * 2 / CIRCLE_SVG_SIZE
        };

        this.foodSprite.tint = color;
    }

    render() {
        return this.foodSprite;
    }
}

export default Food;