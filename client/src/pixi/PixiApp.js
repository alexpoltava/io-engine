import _ from "lodash";
import * as PIXI from "pixi.js";
import Player from "./objects/Player";
import Food from "./objects/Food";
import Leaderboard from "./objects/Leaderboard";

import {
  BACKGROUND_COLOR,
  SEND_DATA_INTERVAL,
  SEND_WINDOW_SIZE_INTERVAL,
} from "../config";

// import { FAKE_DATA, drawCrossHair } from "../mock";

class PixiApp {
  constructor({ pixiRoot, onEvent, onTick }) {
    this.pixiRoot = pixiRoot;
    this.onEvent = onEvent;
    this.onTick = onTick;
    this.colors = [];

    this.app = new PIXI.Application({
      backgroundColor: BACKGROUND_COLOR,
      resizeTo: pixiRoot,
    });
    pixiRoot.appendChild(this.app.view);

    this.setupEvents();

    this.initApp();
  }

  setColors = (colors) => {
    this.colors = colors;
  };

  setupEvents = () => {
    window.addEventListener(
      "resize",
      _.debounce(this.onResize, SEND_WINDOW_SIZE_INTERVAL)
    );

    this.app.renderer.plugins.interaction.on(
      "pointermove",
      _.throttle(this._handleMove.bind(this), SEND_DATA_INTERVAL)
    );
    this.app.renderer.plugins.interaction.on(
      "pointerdown",
      this._handleMove.bind(this)
    );
  };

  _handleMove = (e) => {
    e.stopPropagation();
    const data = {
      force: {
        x: e.data.global.x - this.app.screen.width / 2,
        y: this.app.screen.height / 2 - e.data.global.y,
      },
    };
    this.onEvent(data);
  };

  onResize = () => {
    this.sendWindowSize();
    this.updateSize();
  };

  sendWindowSize = () => {
    const data = {
      screen: {
        width: this.app.screen.width,
        height: this.app.screen.height,
      },
    };
    this.onEvent(data);
  };

  updateSize = () => {
    this.tilingSprite.width = this.app.screen.width;
    this.tilingSprite.height = this.app.screen.height;
  };

  initApp = () => {
    // background
    const tile = PIXI.Texture.from("assets/tile.png");
    this.tilingSprite = new PIXI.TilingSprite(
      tile,
      this.app.screen.width,
      this.app.screen.height
    );
    this.app.stage.addChild(this.tilingSprite);

    // food container
    const options = {
      scale: true,
      position: true,
      rotation: true,
      uvs: true,
      alpha: true,
    };

    this.foodContainer = new PIXI.ParticleContainer(1000, options);
    this.app.stage.addChild(this.foodContainer);

    // players container
    this.playersContainer = new PIXI.Container();
    this.app.stage.addChild(this.playersContainer);

    // info
    this.info = new PIXI.Text("", {
      fontFamily: "Arial",
      fontSize: 16,
      align: "left",
      fill: 0xa0a0a0,
    });
    this.info.x = 24;
    this.info.y = 24;
    this.app.stage.addChild(this.info);

    // leaderboard
    this.leaderboard = new Leaderboard({
      x: this.app.screen.width,
      y: 0,
    });
    this.app.stage.addChild(this.leaderboard.render());

    this.app.ticker.add(this.onTick);
  };

  composeScene = (data, statistics, appSettings) => {
    if (data) {
      const { width, height } = this.app.screen;

      // food
      this.foodContainer.removeChildren();
      for (const food of data.food) {
        const foodObj = new Food({
          x: food.position.x + width / 2,
          y: height / 2 - food.position.y,
          radius: food.radius,
          color: this.colors[food.color],
        });

        this.foodContainer.addChild(foodObj.render());
      }

      // players
      this.playersContainer.removeChildren();
      for (const player of data.players) {
        const playerObj = new Player({
          name: player.name,
          x: player.position.x + width / 2,
          y: height / 2 - player.position.y,
          radius: player.radius,
          color: this.colors[player.color],
        });

        this.playersContainer.addChild(playerObj.render());
      }

      //  background
      this.tilingSprite.tilePosition.x = -data.center.x + width / 2;
      this.tilingSprite.tilePosition.y = data.center.y + height / 2;

      // info
      if (appSettings && appSettings.info) {
        const me = data.players.find((player) => player.me);

        this.info.text =
          "food visible: " +
          data.food.length +
          "\nplayers visible: " +
          data.players.length +
          "\nposition: " +
          Math.round(data.center.x) +
          ", " +
          Math.round(data.center.y) +
          (me
            ? "\nmass: " +
            me.mass / 10 +
            "\nradius: " +
            me.radius +
            "\nspeed: " +
            (me.speed / 10).toFixed(1)
            : "");
      } else {
        this.info.text = "";
      }

      // leaderboard
      this.leaderboard.update({
        x: this.app.screen.width,
        y: 0,
        statistics,
      });
    }
  };

  drawScene = (frameData, statistics, appSettings) => {
    this.composeScene(frameData, statistics, appSettings);

    // this.composeScene(FAKE_DATA);
    // drawCrossHair(this.app, PIXI);
  };
}

export default PixiApp;
