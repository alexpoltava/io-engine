import PixiApp from "./PixiApp";
import { calculateFrameData, get32BitTimestamp } from "../util";
import {
  LOCAL_STATE_UPDATE_INTERVAL,
  DELAY,
  STATES_TO_STORE,
  DATAFRAME,
} from "../config";

class Game {
  constructor({ ws, pixiRoot, appUpdateInGameState }) {
    this.pixiApp = new PixiApp({
      pixiRoot,
      onEvent: this.onEvent,
      onTick: this.onTick
    });
    ws.subscribeToMessages(this.onMessage);

    this.serverData = [];
    this.lastStatistics = [];
    this.prev = {};
    this.next = {};
    this.delta = null;
    this.inGame = false;
    this.appUpdateInGameState = appUpdateInGameState;
    this.ws = ws;

    this.gameStateUpdater = setInterval(() => {
      this.updateLocalGameState();
    }, LOCAL_STATE_UPDATE_INTERVAL);
  }

  onEvent = data => {
    this.ws.send(data);
  };

  joinGame = name => {
    const data = {
      join: { name }
    };
    this.ws.send(data);
    this.pixiApp.onResize();
  };

  onMessage = fullMessage => {
    const { messageType, ...message } = fullMessage;

    switch (messageType) {
      case DATAFRAME.INIT: {
        this.pixiApp.setColors(message.colors);
        return;
      }
      case DATAFRAME.REGULAR:
      case DATAFRAME.REGULAR_WITH_STATISCTICS: {
        // map message
        if (this.delta === null) {
          this.delta = get32BitTimestamp() - message.timestamp;
        }
        const me = message.players.find(player => player.me);

        if (this.inGame !== me.inGame) {
          this.appUpdateInGameState(Boolean(me.inGame));
        }

        this.updateLocalData(message);
        return;
      }
      default: {
        return;
      }
    }
  };

  updateAppSettings = appSettings => {
    this.appSettings = appSettings;
  }

  updateInGameState = inGame => {
    this.inGame = inGame;
  };

  updateLocalData = message => {
    if (message.hasOwnProperty("statistics")) {
      this.lastStatistics = message.statistics;
    }
    this.serverData = [
      message,
      ...this.serverData.slice(0, STATES_TO_STORE - 1)
    ];
  };

  getBoundaryStates = now => {
    const prev = this.serverData.findIndex(data => {
      return data.timestamp <= now;
    });
    return [this.serverData[prev], this.serverData[prev - 1]];
  };

  updateLocalGameState = () => {
    const now = get32BitTimestamp() - DELAY - this.delta;

    const [_prev, _next] = this.getBoundaryStates(now);

    if (_prev && _next) {
      if (
        _prev.timestamp !== this.prev.timestamp ||
        _next.timestamp !== this.next.timestamp
      ) {
        this.prev = _prev;
        this.next = _next;
      }
    }
  };

  onTick = _ => {
    const { prev, next } = this;
    const frameData = calculateFrameData(prev, next, this.delta, DELAY);
    this.pixiApp.drawScene(frameData, this.lastStatistics, this.appSettings);
  };
}

export default Game;
