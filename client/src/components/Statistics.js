import React, { Component } from 'react';
import { Card, Label, Grid } from 'semantic-ui-react'
import moment from 'moment';
import momentDurationFormatSetup from 'moment-duration-format';

momentDurationFormatSetup(moment);

class Statistics extends Component {
  render() {
    const { data } = this.props;

    return  <Card fluid>
              <Card.Content>
                <Card.Header textAlign='center'>Statistics</Card.Header>
                {data && Object.keys(data).length
                  ? <Card.Description>
                     <Grid columns={2}>
                      <Grid.Column>
                        <p>Games played: {Object.keys(data).length}</p>
                        <p>Highest mass: {Math.max(...Object.keys(data).map(game => data[game].highestMass))}</p>
                        <p>Food eaten: {Object.keys(data).reduce((acc, game) => acc + data[game].foodEaten, 0)}</p>
                        <p>Player cells eaten: {Object.keys(data).reduce((acc, game) => acc + data[game].playerCellsEaten, 0)}</p>
                      </Grid.Column>
                      <Grid.Column>
                        <p>Total in game: {moment.duration(Object.keys(data).reduce((acc, game) => acc + (data[game].gameEnded - data[game].gameStarted), 0)).format("d[d] HH[h] mm[m] ss[s]")}</p>
                      </Grid.Column>
                      </Grid>
                     </Card.Description>
                  : <Card.Description>
                      <Label>No data</Label>
                    </Card.Description>}
              </Card.Content>
            </Card>;
  }
}

export default Statistics;
