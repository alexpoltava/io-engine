import React, { Component } from 'react';
import { toast } from 'react-toastify';
import { Card, Image } from 'semantic-ui-react';
import firebase from '../service/firebase';
import StyledFirebaseAuth from 'react-firebaseui/StyledFirebaseAuth';
import { auth } from '../service/firebase';

const toastOptions = {
  autoClose: 3000,
  type: toast.TYPE.ERROR,
  hideProgressBar: false,
  position: toast.POSITION.TOP_RIGHT,
  pauseOnHover: true,
};

const uiConfig = {
  signInFlow: 'popup',
  autoUpgradeAnonymousUsers: true,
  signInOptions: [
    firebase.auth.GoogleAuthProvider.PROVIDER_ID,
  ],
  callbacks: {
    signInSuccessWithAuthResult: function (authResult, redirectUrl) {
      window.location.reload();
      return false;
    },
    signInFailure: function (error) {
      if (error.code === 'firebaseui/anonymous-upgrade-merge-conflict') {
        console.log('User already exists, try different account');
        toast('User already exists, try different account', toastOptions);
        return Promise.reject();
      } else {
        return Promise.resolve();
      }
    }
  }
};

class AccountInfo extends Component {
  render() {
    return <Card fluid>
      <Card.Content>
        <Card.Header textAlign='center'>Account Info</Card.Header>
        {auth.currentUser.isAnonymous
          ? <Card.Description>
            <p>You are logged in as anonymous user</p>
            <p>Link your account:</p>
            <StyledFirebaseAuth uiConfig={uiConfig} firebaseAuth={auth} />
          </Card.Description>
          : <Card.Description>
            <p>{`Hello, `}
              <Image src={auth.currentUser.providerData[0].photoURL} size='mini' alt='avatar' avatar />
              {`${auth.currentUser.providerData[0].displayName}!`}</p>
            <p>
              {`You are logged in with `}
              <a href={`mailto:${auth.currentUser.providerData[0].email}`}>{auth.currentUser.providerData[0].email}</a>
            </p>
            <p>{`Provider: ${auth.currentUser.providerData[0].providerId}`}</p>
          </Card.Description>}
      </Card.Content>
    </Card>;
  }
}

export default AccountInfo;
