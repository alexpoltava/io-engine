import React, { Component } from 'react';
import { Button, Icon, Form, Card } from 'semantic-ui-react'

class StartGame extends Component {
  constructor(props) {
    super(props);
    
    this.state = {
      name: props.name || ''
    }
  }
  
  _handleChange = (e) => {
    this.setState({ [e.target.name]: e.target.value });
  }
  
  _handlePlay = () => {
    const { name } = this.state;
    if(name !== this.props.name) {
      this.props.updateSettings({ name })
      .then(() => {
        this.props.play(name);  
      })
      .catch(e => console.log(e.message));
    } else {
      this.props.play(name);      
    }
  }

  render() {
    const { isConnecting } = this.props;
    return <Card fluid>
            <Card.Content>
              <Card.Header textAlign='center'>Game</Card.Header>
              <Card.Description>
                <Form onSubmit={this._handlePlay}>
                  <Form.Input
                    onChange={this._handleChange}
                    name="name"
                    value={this.state.name}
                    placeholder="name"
                    disabled={isConnecting}
                  />
                    <Button
                      type='submit'
                      inverted
                      color='green'
                      style={{ width: "100%" }}
                      floated="right"
                      disabled={this.state.name === '' || isConnecting}
                    >
                      <Icon name='checkmark' />
                      Play!
                    </Button>
                </Form>
              </Card.Description>
            </Card.Content>
          </Card>;
  }
}

export default StartGame;
