import React, { Component } from 'react';
import { Button } from 'semantic-ui-react'
import { auth } from '../service/firebase';

class SignOut extends Component {
  render() {
    return  <Button
              basic
              color='orange'
              floated='right'
              icon='sign out'
              content='Sign out'
              onClick={() => {
                auth.signOut()
                  .then(() => {})
                  .catch(e => console.log(e));
              }}
            />;
  }
}

export default SignOut;
