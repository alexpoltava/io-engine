import React, { Component } from 'react';
import { Form, Card, Checkbox } from 'semantic-ui-react'

class Settings extends Component {
  render() {
    const { settings, updateSettings } = this.props;

    return <Card fluid>
      <Card.Content>
        <Card.Header textAlign='center'>Settings</Card.Header>
        <Card.Description>
          <Form>
            <Form.Field>
              <Checkbox
                label='show info'
                checked={settings.info}
                toggle
                onChange={(e, data) => updateSettings({
                  info: data.checked
                })}
              />
            </Form.Field>
            <Form.Field>
              <Checkbox
                label='show statistics'
                checked={settings.statistics}
                toggle
                onChange={(e, data) => updateSettings({
                  statistics: data.checked
                })}
              />
            </Form.Field>
          </Form>
        </Card.Description>
      </Card.Content>
    </Card>;
  }
}

export default Settings;
