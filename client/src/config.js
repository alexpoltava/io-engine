export const SEND_DATA_INTERVAL = 33;
export const SEND_WINDOW_SIZE_INTERVAL = 200;
export const STATES_TO_STORE = 20;
export const DELAY = 100; // ms
export const LOCAL_STATE_UPDATE_INTERVAL = 33;
export const CIRCLE_SVG_SIZE = 1600;
export const BACKGROUND_COLOR = 0xf0f0f0;
export const GAME_SERVER =
  window.location.hostname === "localhost"
    ? "ws://localhost:8000"
    : "wss://app-dev.nl:8000";
export const DATAFRAME = Object.freeze({
  REGULAR: 0,
  INIT: 1,
  REGULAR_WITH_STATISCTICS: 2
});
