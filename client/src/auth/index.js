import { auth } from '../service/firebase';

export const addAuthStateChanged = ({ setIsLoggedIn, wsOpen, wsClose }) =>
  auth.onAuthStateChanged(
    user => {
      if (user) {
        auth.currentUser.getIdToken(/* forceRefresh */ true)
          .then(idToken => {
            setIsLoggedIn(user.uid);
            wsOpen(idToken);
          }).catch(e => {
            console.log(e.message);
          })
      } else {
        setIsLoggedIn(false)
        wsClose();
      }
    }
);
