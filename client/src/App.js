import React, { Component, Fragment } from "react";
import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import Connection from "./lib/Connection";
import Game from "./pixi/Game";
import StartPage from "./containers/StartPage";
import Version from "./components/Version";
import { addAuthStateChanged } from "./auth";

class App extends Component {
  Game = null;
  state = {
    inGame: false,
    isConnecting: true,
    isLoggedIn: false,
    settings: {
      name: "",
      statistics: false,
      info: false
    },
  };

  handlePlay = name => {
    this.Game.joinGame(name);
  };

  componentDidMount() {
    const ws = new Connection({
      subscriber: console.log,
      onOpenCb: () => this.setState({ isConnecting: false }),
      onCloseCb: () => this.setState({ isConnecting: false })
    });

    this.unsubscribe = addAuthStateChanged({
      setIsLoggedIn: state => this.setState({ isLoggedIn: state }),
      wsOpen: this.wsOpen.bind(null, ws),
      wsClose: this.wsClose.bind(null, ws)
    });

    this.Game = new Game({
      ws,
      pixiRoot: this.pixiRoot,
      appUpdateInGameState: this.updateInGameState
    });
  }

  componentWillUnmount() {
    this.unsubscribe && this.unsubscribe();
  }

  wsOpen = (ws, idToken) => {
    this.setState({
      isConnecting: true
    });
    ws.open(idToken);
  };

  wsClose = ws => {
    ws.close();
    this.updateLocalSettings({
      name: "",
      statistics: false,
      info: false
    });
  };

  updateInGameState = inGame => {
    this.Game.updateInGameState(inGame);
    this.setState({
      inGame
    });
  };

  updateLocalSettings = settings => {
    this.Game.updateAppSettings(settings);
    this.setState({
      settings
    });
  };

  setPixiRoot = el => {
    this.pixiRoot = el;
  };

  render() {
    const { isConnecting, isLoggedIn, inGame, settings } = this.state;

    return (
      <Fragment>
        <div ref={this.setPixiRoot} style={{ height: "100vh" }} />
        <StartPage
          inGame={inGame}
          isConnecting={isConnecting}
          play={this.handlePlay}
          isLoggedIn={isLoggedIn}
          settings={settings}
          updateLocalSettings={this.updateLocalSettings}
        />
        <Version />
        <ToastContainer />
      </Fragment>
    );
  }
}

export default App;
