import { objectDeserialize } from '../lib/msg-pack';
import { getMessageDefinition } from '../service/message-definition';
import { mockMessage0, mockFrame0, mockMessage1, mockFrame1, mockMessage2, mockFrame2, mockCustomFrame } from '../mock';


test('should deserialize regular message correctly', () => {
    const data = Buffer.from(mockFrame0, 'hex');
    const messageType = data.readUInt8(0);
    const messageBuffer = data.slice(1);
    const message = objectDeserialize(messageBuffer, getMessageDefinition(messageType)).object;
    expect(message).toEqual(mockMessage0);
})

test('should deserialize init message correctly', () => {
    const data = Buffer.from(mockFrame1, 'hex');
    const messageType = data.readUInt8(0);
    const messageBuffer = data.slice(1);
    const message = objectDeserialize(messageBuffer, getMessageDefinition(messageType)).object;
    expect(message).toEqual(mockMessage1);
})

test('should deserialize regular with statistics message correctly', () => {
    const data = Buffer.from(mockFrame2, 'hex');
    const messageType = data.readUInt8(0);
    const messageBuffer = data.slice(1);
    const message = objectDeserialize(messageBuffer, getMessageDefinition(messageType)).object;
    expect(message).toEqual(mockMessage2);
})

test('should deserialize custom frame correctly', () => {
    const data = Buffer.from(mockCustomFrame, 'hex');
    const messageType = data.readUInt8(0);
    const messageBuffer = data.slice(1);
    objectDeserialize(messageBuffer, getMessageDefinition(messageType)).object;
    expect.not.toThrow();
})
