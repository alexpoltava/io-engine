import React, { Component } from 'react';
import { Modal, Loader } from 'semantic-ui-react'
import firebase from '../service/firebase';
import StyledFirebaseAuth from 'react-firebaseui/StyledFirebaseAuth';
import * as firebaseui from 'firebaseui';
import { auth } from '../service/firebase';
import PrivateUserLayout from './PrivateUserLayout';

// Configure FirebaseUI.
const uiConfig = {
  // Popup signin flow rather than redirect flow.
  signInFlow: 'popup',
  // We will display Google and Facebook as auth providers.
  signInOptions: [
    firebase.auth.GoogleAuthProvider.PROVIDER_ID,
    firebaseui.auth.AnonymousAuthProvider.PROVIDER_ID,
  ],
};

class StartPage extends Component {
  render() {
    const { inGame, isConnecting, isLoggedIn, play, updateLocalSettings, settings } = this.props;
    return <Modal
      open={!inGame}
      basic
      size='tiny'
      closeOnDimmerClick={false}
    >
      <Modal.Content>
        {isConnecting
          ? <Loader />
          : (isLoggedIn
            ? <PrivateUserLayout
              isConnecting={isConnecting}
              userId={isLoggedIn}
              play={play}
              updateLocalSettings={updateLocalSettings}
              settings={settings}
            />
            : <StyledFirebaseAuth uiConfig={uiConfig} firebaseAuth={auth} />)}
      </Modal.Content>
    </Modal>;
  }
}

export default StartPage;
