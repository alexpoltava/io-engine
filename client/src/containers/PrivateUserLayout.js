import React, { Component } from 'react';
import { Loader, Grid, Container, Transition } from 'semantic-ui-react'
import { db, auth } from '../service/firebase';
import StartGame from '../components/StartGame';
import Settings from '../components/Settings';
import Statistics from '../components/Statistics';
import AccountInfo from '../components/AccountInfo';
import SignOut from '../components/SignOut';

class PrivateUserLayout extends Component {
  state = {
    statistics: {},
    isLoading: true,
  };

  componentDidMount() {
    this.connectToDatabase();
  }

  componentWillUnmount() {
    this.disconnectFromDatabase();
  }

  disconnectFromDatabase() {
    const { userId } = this.props;
    const user = db.ref(`users/${userId}`);

    user.off("value");
  }

  connectToDatabase() {
    const { userId } = this.props;
    const user = db.ref(`users/${userId}`);

    user.on("value", snapshot => {
      const userData = snapshot.val();
      if (userData) {
        this.props.updateLocalSettings({
          ...userData.settings
        });
        this.setState({
          statistics: userData.statistics
        })
      }
      this.setState({
        isLoading: false,
      });
    });
  }

  updateSettings = (settings) => {
    const { userId } = this.props;

    const user = db.ref(`users/${userId}/settings`);

    return user.update(settings);
  }

  render() {
    const { isConnecting, play, settings } = this.props;
    const { isLoading, statistics } = this.state;

    return isLoading
      ? <Loader />
      : <Container>
        <Grid columns={2}>
          <Grid.Row stretched>
            <Grid.Column>
              <StartGame
                updateSettings={this.updateSettings}
                isConnecting={isConnecting}
                play={play}
                name={settings.name}
              />
            </Grid.Column>
            <Grid.Column>
              <Settings
                settings={settings}
                updateSettings={this.updateSettings}
              />
            </Grid.Column>
          </Grid.Row>
        </Grid>
        <Transition visible={settings.statistics === true}>
          <div style={{ marginTop: '1rem' }}>
            <Statistics
              data={statistics}
            />
          </div>
        </Transition>
        {auth.currentUser && <AccountInfo />}
        <SignOut />
      </Container>;
  }
}

export default PrivateUserLayout;
