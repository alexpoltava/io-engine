import ReconnectingWebSocket from "reconnecting-websocket";
import { GAME_SERVER } from "../config";
import { objectDeserialize, getMessageDefinition } from './msg-pack';

import zlib from "zlib";

class Connection {
  constructor({ subscriber, onOpenCb, onCloseCb }) {
    this.subscriber = subscriber;
    this.onOpenCb = onOpenCb;
    this.onCloseCb = onCloseCb;
    this.onOpen = this.onOpen.bind(this);
    this.onMessage = this.onMessage.bind(this);
  }

  onOpen = () => {
    console.log("onOpen");
    this.onOpenCb && this.onOpenCb();
  };

  onClose = () => {
    console.log("onClose");
    this.onCloseCb && this.onCloseCb();
  };

  onMessage = msg => {
    if (typeof msg.data === "object") {
      zlib.unzip(Buffer.from(msg.data), (e, data) => {
        if (e) throw e;

        const messageType = data.readUInt8(0);
        const messageBuffer = data.slice(1);
        const messageDefinition = getMessageDefinition(messageType);
        const deserializedMessage = objectDeserialize(messageBuffer, messageDefinition);
        const message = {
          ...(deserializedMessage.object),
          messageType
        };
        if (this.subscriber) this.subscriber(message);
      });
    }
  };

  open = idToken => {
    this.socket = new ReconnectingWebSocket(
      `${GAME_SERVER}?token=${idToken}`,
      [],
      { startClosed: false }
    );
    this.socket.binaryType = "arraybuffer";
    this.socket.addEventListener("open", this.onOpen);
    this.socket.onclose = this.onOpen;
    this.socket.addEventListener("message", this.onMessage);
    this.socket.reconnect();
  };

  close = () => {
    console.log("close");
    if (this.socket) {
      this.socket.close();
      this.socket = null;
    }
    this.onCloseCb && this.onCloseCb();
  };

  send = data => {
    if (this.socket) {
      this.socket.send(Buffer.from(JSON.stringify(data)));
    }
  };

  subscribeToMessages = fn => {
    this.subscriber = fn;
  };
}

export default Connection;
