import { DATAFRAME } from "../config";

const coordinate = {
  type: "FloatBE",
  size: 4,
};

const coordinatesXY = [
  {
    ...coordinate,
    name: "x",
  },
  {
    ...coordinate,
    name: "y",
  },
];

const regularMessage = [
  {
    name: "center",
    type: "object",
    definition: coordinatesXY,
  },
  {
    name: "players",
    type: "array",
    elementType: "object",
    elementDefinition: [
      {
        name: "id",
        type: "UInt16BE",
        size: 2,
      },
      {
        name: "color",
        type: "UInt8",
        size: 1,
      },
      {
        name: "radius",
        type: "UInt16BE",
        size: 2,
      },
      {
        name: "inGame",
        type: "UInt8",
        size: 1,
      },
      {
        name: "mass",
        type: "UInt16BE",
        size: 2,
      },
      {
        name: "speed",
        type: "UInt16BE",
        size: 2,
      },
      {
        name: "me",
        type: "UInt8",
        size: 1,
      },
      {
        name: "name",
        type: "string",
      },
      {
        name: "position",
        type: "object",
        definition: coordinatesXY,
      },
    ],
  },
  {
    name: "food",
    type: "array",
    elementType: "object",
    elementDefinition: [
      {
        name: "id",
        type: "UInt16BE",
        size: 2,
      },
      {
        name: "color",
        type: "UInt8",
        size: 1,
      },
      {
        name: "radius",
        type: "UInt16BE",
        size: 2,
      },
      {
        name: "position",
        type: "object",
        definition: coordinatesXY,
      },
    ],
  },
  {
    name: "timestamp",
    type: "UInt32BE",
    size: 4,
  },
  {
    name: "center",
    type: "object",
    definition: coordinatesXY,
  },
];

const regularMessageWithStatistics = [
  ...regularMessage,
  {
    name: "statistics",
    type: "array",
    elementType: "object",
    elementDefinition: [
      {
        name: "name",
        type: "string",
      },
      {
        name: "place",
        type: "UInt8",
        size: 1,
      },
      {
        name: "mass",
        type: "UInt16BE",
        size: 2,
      },
      {
        name: "me",
        type: "UInt8",
        size: 1,
      },
    ],
  },
];

const initMessage = [
  {
    name: "colors",
    type: "array",
    elementType: "UInt32BE",
    elementSize: 4,
  },
];

export const getMessageDefinition = (messageType) => {
  switch (messageType) {
    case DATAFRAME.REGULAR: {
      return regularMessage;
    }
    case DATAFRAME.INIT: {
      return initMessage;
    }
    case DATAFRAME.REGULAR_WITH_STATISCTICS: {
      return regularMessageWithStatistics;
    }
    default: {
      throw new Error(`Unknown message Type: ${messageType}`);
    }
  }
};

const fieldSerialize = (value, type, size) => {
  switch (type) {
    case "UInt8": {
      const buff = Buffer.alloc(size);
      buff.writeUInt8(value);
      return buff;
    }
    case "FloatBE": {
      const buff = Buffer.alloc(size);
      buff.writeFloatBE(value);
      return buff;
    }
    case "Int16BE": {
      const buff = Buffer.alloc(size);
      buff.writeInt16BE(value);
      return buff;
    }
    case "UInt16BE": {
      const buff = Buffer.alloc(size);
      buff.writeUInt16BE(value);
      return buff;
    }
    case "UInt32BE": {
      const buff = Buffer.alloc(size);
      buff.writeUInt32BE(value);
      return buff;
    }
    case "hex": {
      const buff = Buffer.from(value, "hex");
      return buff;
    }
    case "string": {
      const length = Buffer.alloc(1);
      length.writeUInt8(value.length);
      return Buffer.concat([length, Buffer.from(value, "UTF-8")]);
    }
    default: {
      throw new Error(`Type ${type} not found`);
    }
  }
};

export const objectSerialize = (data, messageDefinition) => {
  let buff = Buffer.alloc(0);
  for (const field of messageDefinition) {
    if (!data.hasOwnProperty(field.name)) {
      console.log("data", data);
      console.log("messageDefinition", messageDefinition);
      throw new Error(`Field ${field.name} not found`);
    }
    if (field.type === "object") {
      buff = Buffer.concat([
        buff,
        objectSerialize(data[field.name], field.definition),
      ]);
    } else if (field.type === "array") {
      const length = Buffer.alloc(2);
      length.writeUInt16BE(data[field.name].length);
      buff = Buffer.concat([buff, length]);

      for (const element of data[field.name]) {
        if (field.elementType === "object") {
          buff = Buffer.concat([
            buff,
            objectSerialize(element, field.elementDefinition),
          ]);
        } else {
          buff = Buffer.concat([
            buff,
            fieldSerialize(element, field.elementType, field.elementSize),
          ]);
        }
      }
    } else {
      buff = Buffer.concat([
        buff,
        fieldSerialize(data[field.name], field.type, field.size),
      ]);
    }
  }
  return buff;
};

export const fieldDeserialize = (buff, type) => {
  switch (type) {
    case "UInt8": {
      return buff.readUInt8(0);
    }
    case "FloatBE": {
      return buff.readFloatBE(0);
    }
    case "Int16BE": {
      return buff.readInt16BE(0);
    }
    case "UInt16BE": {
      return buff.readUInt16BE(0);
    }
    case "UInt32BE": {
      return buff.readUInt32BE(0);
    }
    case "hex": {
      return buff.toString("hex");
    }
    case "string": {
      return buff.toString();
    }
    default: {
      throw new Error(`Type ${type} not found`);
    }
  }
};

export const objectDeserialize = (buff, messageDefinition) => {
  let data = {};
  let pointer = 0;
  for (const field of messageDefinition) {
    if (field.type === "object") {
      const { endPointer, object } = objectDeserialize(
        buff.slice(pointer),
        field.definition
      );
      data = {
        ...data,
        [field.name]: object,
      };
      pointer += endPointer;
    } else if (field.type === "array") {
      const length = buff.slice(pointer, pointer + 2).readUInt16BE(0);
      pointer += 2;
      let arr = [];
      for (let i = 0; i < length; i++) {
        if (field.elementType === "object") {
          const { endPointer, object } = objectDeserialize(
            buff.slice(pointer),
            field.elementDefinition
          );
          arr = [...arr, object];
          pointer += endPointer;
        } else if (field.elementType === "string") {
          const length = buff.slice(pointer, pointer + 1).readUInt8(0);
          pointer += 1;
          arr = [
            ...arr,
            fieldDeserialize(buff.slice(pointer, pointer + length), field.type),
          ];
          pointer += length;
        } else {
          arr = [
            ...arr,
            fieldDeserialize(
              buff.slice(pointer, pointer + field.elementSize),
              field.elementType
            ),
          ];
          pointer += field.elementSize;
        }
      }
      data = { ...data, [field.name]: arr };
    } else if (field.type === "string") {
      const length = buff.slice(pointer, pointer + 1).readUInt8(0);
      pointer += 1;
      data = {
        ...data,
        [field.name]: fieldDeserialize(
          buff.slice(pointer, pointer + length),
          field.type
        ),
      };
      pointer += length;
    } else {
      data = {
        ...data,
        [field.name]: fieldDeserialize(
          buff.slice(pointer, pointer + field.size),
          field.type
        ),
      };
      pointer += field.size;
    }
  }
  return {
    endPointer: pointer,
    object: data,
  };
};
