const admin = require('firebase-admin');

const serviceAccount = require(process.env.serviceAccount);

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: "https://io-engine-a6fb0.firebaseio.com"
});

const auth = admin.auth();
const db = admin.database();

module.exports = {
  auth,
  db,
}
