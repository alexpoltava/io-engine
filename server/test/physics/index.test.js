const { GameWorld } = require('../../physics');

test('create GameWorld correctly', () => {
    const world = new GameWorld();
    expect(world).toHaveProperty('createWorldBoundaries');
    expect(world).toHaveProperty('addPlayer');
    expect(world).toHaveProperty('addFood');
    expect(world.getWorldState().bodies.filter(body => body.label === 'boundary').length).toBe(4);
});

test('add player correctly', () => {
    const world = new GameWorld();
    world.addPlayer('someId');
    const players = world.getWorldState().bodies.filter(body => body.label === 'player');
    expect(players.length).toBe(1);
});

test('add food correctly', () => {
    const FOOD_COUNT = 1000;
    const world = new GameWorld();
    const arr = Array.from(Array(FOOD_COUNT));
    for (const el of arr) {
        world.addFood();
    }
    const food = world.getWorldState().bodies.filter(body => body.label === 'food');
    expect(food.length).toBe(FOOD_COUNT);
    expect(world.getFoodCount()).toBe(FOOD_COUNT);
});

test('add many food correctly', () => {
    const FOOD_COUNT = 1000;
    const world = new GameWorld();
    world.addFood(FOOD_COUNT);
    const food = world.getWorldState().bodies.filter(body => body.label === 'food');
    expect(food.length).toBe(FOOD_COUNT);
    expect(world.getFoodCount()).toBe(FOOD_COUNT);
});

test('player, set screen properties correctly', () => {
    const world = new GameWorld();
    const CLIENT_ID = 'asdf';
    const WIDTH = 1501;
    const HEIGHT = 1382;
    world.addPlayer(CLIENT_ID);
    const player = world.getPlayerByClientId(CLIENT_ID);
    world.updatePlayerScreen(player, WIDTH, HEIGHT);
    expect(player.plugin.screen.width).toBe(WIDTH);
    expect(player.plugin.screen.height).toBe(HEIGHT);
    expect(player.plugin.clientId).toBe(CLIENT_ID);
});
