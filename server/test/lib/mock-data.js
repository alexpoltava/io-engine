
const initMessage = {
    colors: [
        13386708, 14267744, 7621130, 3387879, 12967070, 4318165,
        14167212, 4335891, 2771733, 3502507, 5500392, 3677651,
        7829370, 16758908, 1146251, 7566343, 8864119, 13889497,
        16735902, 10034327, 10723643, 13557111, 13772713, 14746823,
        1493280, 9552300, 14636692, 1976423, 12892949, 9977526,
        16256716, 7792192, 1320201, 2104078, 752672, 5716391,
        8030160, 2163938, 6018782, 1350081, 4098207, 4723248,
        15646736, 16491750, 920624, 3172325, 12765459, 11841784,
        2702521, 1524921, 2512050, 15621943, 2333092, 13352383,
        464482, 9522478, 4769381, 3556312, 12970285, 452454,
        3362054, 5434596, 14744516, 1244226, 10969688, 9986640,
        399410, 13403160, 13595445, 1415836, 1483852, 12578219,
        10802601, 12051887, 9241081, 8802671, 4410268, 10887444,
        15595679, 6458202, 4222975, 12631209, 15976540, 15266009,
        15218001, 2110348, 14815165, 792812, 8775024, 9825470,
        8583569, 8431053, 16490523, 13835742, 15095467, 3061088,
        11582338, 6486406, 1426519, 8422985
    ]
};

const regularMessage = {
    players: [
        {
            id: 'aca72685',
            color: 163,
            radius: 1,
            inGame: 0,
            name: '',
            mass: 2,
            me: 1,
            position: { x: 0, y: 0 }
        },
    ],
    food: [
        {
            id: '5917bb',
            color: 119,
            radius: 1,
            position: { x: -32, y: -545 }
        },
        {
            id: '3054d7',
            color: 245,
            radius: 1,
            position: { x: -213, y: -14 }
        },
        {
            id: 'fac84b',
            color: 90,
            radius: 1,
            position: { x: -902, y: 71 }
        },
        {
            id: 'b4b315',
            color: 158,
            radius: 1,
            position: { x: 298, y: -668 }
        },
        {
            id: '035c04',
            color: 70,
            radius: 1,
            position: { x: 463, y: -414 }
        },
        {
            id: '90db10',
            color: 10,
            radius: 1,
            position: { x: 553, y: 405 }
        },
        {
            id: '313699',
            color: 124,
            radius: 1,
            position: { x: 458, y: 101 }
        },
        {
            id: '1d6de8',
            color: 7,
            radius: 1,
            position: { x: -757, y: 296 }
        },
        {
            id: '2ec9a5',
            color: 53,
            radius: 1,
            position: { x: -28, y: 842 }
        },
        {
            id: '74e16c',
            color: 239,
            radius: 1,
            position: { x: -900, y: -837 }
        },
        {
            id: '006563',
            color: 43,
            radius: 1,
            position: { x: -237, y: 737 }
        },
        {
            id: 'c60e94',
            color: 232,
            radius: 1,
            position: { x: -20, y: 166 }
        },
        {
            id: 'b7cb57',
            color: 192,
            radius: 1,
            position: { x: 843, y: 949 }
        },
        { id: '492449', color: 2, radius: 1, position: { x: 969, y: 890 } },
        {
            id: '28c836',
            color: 109,
            radius: 1,
            position: { x: -966, y: -232 }
        },
        {
            id: '6a40f5',
            color: 161,
            radius: 1,
            position: { x: 789, y: 479 }
        },
        { id: 'e78e39', color: 115, radius: 1, position: { x: 815, y: 2 } },
        {
            id: '6577a2',
            color: 177,
            radius: 1,
            position: { x: -767, y: 210 }
        },
        {
            id: '9f1b7c',
            color: 243,
            radius: 1,
            position: { x: 321, y: -171 }
        },
        {
            id: '524b2e',
            color: 49,
            radius: 1,
            position: { x: 115, y: 822 }
        },
        {
            id: '7f0264',
            color: 248,
            radius: 1,
            position: { x: 944, y: 404 }
        },
        {
            id: '57d053',
            color: 162,
            radius: 1,
            position: { x: -681, y: -25 }
        },
        {
            id: 'f25860',
            color: 158,
            radius: 1,
            position: { x: -62, y: 147 }
        },
        {
            id: 'a4f88c',
            color: 64,
            radius: 1,
            position: { x: 204, y: -361 }
        },
        {
            id: '292b20',
            color: 167,
            radius: 1,
            position: { x: -76, y: 436 }
        },
        {
            id: '162d8e',
            color: 168,
            radius: 1,
            position: { x: 621, y: 79 }
        },
        {
            id: '43491f',
            color: 236,
            radius: 1,
            position: { x: -627, y: 925 }
        },
        {
            id: '5e6782',
            color: 84,
            radius: 1,
            position: { x: -859, y: -215 }
        },
        {
            id: '893c71',
            color: 229,
            radius: 1,
            position: { x: 777, y: 563 }
        },
        {
            id: 'aa5f10',
            color: 193,
            radius: 1,
            position: { x: -135, y: -490 }
        },
        {
            id: '8dba24',
            color: 72,
            radius: 1,
            position: { x: 362, y: -535 }
        },
        {
            id: '8cbd3a',
            color: 145,
            radius: 1,
            position: { x: 212, y: -857 }
        },
        {
            id: '324593',
            color: 248,
            radius: 1,
            position: { x: -288, y: -429 }
        },
        {
            id: '9b0177',
            color: 150,
            radius: 1,
            position: { x: -753, y: -898 }
        },
        {
            id: '708707',
            color: 139,
            radius: 1,
            position: { x: 699, y: 730 }
        },
        {
            id: '99f959',
            color: 185,
            radius: 1,
            position: { x: 377, y: -738 }
        },
        {
            id: 'c6045c',
            color: 89,
            radius: 1,
            position: { x: -215, y: 389 }
        },
        {
            id: '9c90c1',
            color: 185,
            radius: 1,
            position: { x: -618, y: -720 }
        },
        {
            id: '97b56a',
            color: 151,
            radius: 1,
            position: { x: -794, y: 994 }
        },
        {
            id: 'b811ee',
            color: 190,
            radius: 1,
            position: { x: -342, y: 647 }
        },
        { id: 'b2e219', color: 5, radius: 1, position: { x: 785, y: 344 } },
        {
            id: 'fe9fcb',
            color: 227,
            radius: 1,
            position: { x: -958, y: 273 }
        },
        {
            id: 'e6f998',
            color: 154,
            radius: 1,
            position: { x: 696, y: 598 }
        },
        {
            id: '6fa8b9',
            color: 75,
            radius: 1,
            position: { x: -348, y: -396 }
        },
        {
            id: '43fb20',
            color: 184,
            radius: 1,
            position: { x: -266, y: 914 }
        },
        {
            id: '1474e0',
            color: 209,
            radius: 1,
            position: { x: 347, y: -969 }
        },
        {
            id: '38b774',
            color: 51,
            radius: 1,
            position: { x: -380, y: 839 }
        },
        {
            id: 'd46044',
            color: 42,
            radius: 1,
            position: { x: 272, y: -267 }
        },
        {
            id: 'e16631',
            color: 144,
            radius: 1,
            position: { x: 887, y: 471 }
        },
        {
            id: 'a55eae',
            color: 158,
            radius: 1,
            position: { x: -8, y: -991 }
        },
        {
            id: '7d7fb5',
            color: 129,
            radius: 1,
            position: { x: 974, y: -18 }
        },
        {
            id: 'aef010',
            color: 194,
            radius: 1,
            position: { x: 64, y: 972 }
        },
        {
            id: 'eae115',
            color: 70,
            radius: 1,
            position: { x: 716, y: -982 }
        },
        {
            id: '34ad82',
            color: 177,
            radius: 1,
            position: { x: 762, y: 205 }
        },
        { id: '8af897', color: 5, radius: 1, position: { x: -416, y: 13 } },
        {
            id: '3b22b9',
            color: 112,
            radius: 1,
            position: { x: -837, y: -374 }
        },
        {
            id: '643527',
            color: 184,
            radius: 1,
            position: { x: 907, y: 343 }
        },
        {
            id: '82978d',
            color: 57,
            radius: 1,
            position: { x: -723, y: -811 }
        },
        {
            id: 'd8a487',
            color: 31,
            radius: 1,
            position: { x: -834, y: -769 }
        },
        {
            id: '6f2e8d',
            color: 167,
            radius: 1,
            position: { x: 979, y: 884 }
        },
        {
            id: '9ccbe5',
            color: 137,
            radius: 1,
            position: { x: -20, y: -306 }
        },
        {
            id: '2e4d1e',
            color: 140,
            radius: 1,
            position: { x: -223, y: 407 }
        },
        {
            id: '52426f',
            color: 32,
            radius: 1,
            position: { x: -213, y: 486 }
        },
        {
            id: '0320d3',
            color: 171,
            radius: 1,
            position: { x: 547, y: 852 }
        },
        {
            id: 'dde736',
            color: 196,
            radius: 1,
            position: { x: -623, y: 462 }
        },
        {
            id: '1e7b19',
            color: 223,
            radius: 1,
            position: { x: 762, y: -140 }
        },
        {
            id: 'af0bcc',
            color: 142,
            radius: 1,
            position: { x: 803, y: -785 }
        },
        {
            id: '9891b8',
            color: 10,
            radius: 1,
            position: { x: -180, y: -642 }
        },
        {
            id: 'dc8bf8',
            color: 191,
            radius: 1,
            position: { x: 716, y: -393 }
        },
        {
            id: '70f4f5',
            color: 13,
            radius: 1,
            position: { x: -985, y: 847 }
        },
        {
            id: '7cd206',
            color: 148,
            radius: 1,
            position: { x: 970, y: -883 }
        },
        {
            id: 'a5b550',
            color: 151,
            radius: 1,
            position: { x: 11, y: -815 }
        },
        {
            id: '9b9cba',
            color: 145,
            radius: 1,
            position: { x: -552, y: 157 }
        },
        {
            id: 'd88619',
            color: 79,
            radius: 1,
            position: { x: 441, y: -548 }
        },
    ],
    timestamp: 30706,
    center: { x: 5558, y: 5266 }
}

const regularMessageWithStatistics = {
    players: [
        {
            id: '1240d3d9',
            color: 207,
            radius: 1,
            inGame: 1,
            name: '111',
            mass: 2,
            me: 1,
            position: { x: 0, y: 0 }
        },
    ],
    food: [
        {
            id: 'f78492',
            color: 181,
            radius: 1,
            position: { x: 569, y: -716 }
        },
        {
            id: 'c9a386',
            color: 67,
            radius: 1,
            position: { x: 774, y: 287 }
        },
        {
            id: '0b61db',
            color: 73,
            radius: 1,
            position: { x: -796, y: 763 }
        },
        {
            id: 'a90930',
            color: 173,
            radius: 1,
            position: { x: 759, y: 538 }
        },
        {
            id: '68837c',
            color: 227,
            radius: 1,
            position: { x: 656, y: 378 }
        },
        {
            id: '8469fe',
            color: 170,
            radius: 1,
            position: { x: 263, y: 594 }
        },
        {
            id: 'b1a1aa',
            color: 21,
            radius: 1,
            position: { x: 763, y: -939 }
        },
        {
            id: '3d0ffb',
            color: 120,
            radius: 1,
            position: { x: 856, y: 468 }
        },
        {
            id: '001365',
            color: 244,
            radius: 1,
            position: { x: -338, y: 280 }
        },
        {
            id: '1d3dc6',
            color: 232,
            radius: 1,
            position: { x: -808, y: -398 }
        },
        {
            id: '7c8208',
            color: 172,
            radius: 1,
            position: { x: 818, y: 309 }
        },
        {
            id: 'aac866',
            color: 158,
            radius: 1,
            position: { x: -54, y: -542 }
        },
        {
            id: '7d8d9d',
            color: 34,
            radius: 1,
            position: { x: -870, y: 629 }
        },
        {
            id: 'b6ba8f',
            color: 52,
            radius: 1,
            position: { x: -95, y: -215 }
        },
        {
            id: '7cb967',
            color: 86,
            radius: 1,
            position: { x: -910, y: 345 }
        },
        {
            id: '531703',
            color: 29,
            radius: 1,
            position: { x: 511, y: -169 }
        },
        {
            id: 'a25360',
            color: 75,
            radius: 1,
            position: { x: 713, y: -143 }
        },
        {
            id: 'ebad01',
            color: 160,
            radius: 1,
            position: { x: -255, y: -443 }
        },
        {
            id: '004307',
            color: 198,
            radius: 1,
            position: { x: 998, y: 35 }
        },
        {
            id: 'f229c1',
            color: 243,
            radius: 1,
            position: { x: 607, y: 672 }
        },
        {
            id: 'e53a72',
            color: 251,
            radius: 1,
            position: { x: 657, y: -167 }
        },
        {
            id: '71b9a2',
            color: 105,
            radius: 1,
            position: { x: -592, y: -909 }
        },
        {
            id: 'a86691',
            color: 24,
            radius: 1,
            position: { x: 888, y: 912 }
        },
        {
            id: '625b86',
            color: 23,
            radius: 1,
            position: { x: -266, y: 765 }
        },
        {
            id: '6f5ecc',
            color: 117,
            radius: 1,
            position: { x: 564, y: -381 }
        },
        {
            id: '93e2a6',
            color: 95,
            radius: 1,
            position: { x: -679, y: 156 }
        },
        {
            id: '338ce4',
            color: 103,
            radius: 1,
            position: { x: 655, y: 77 }
        },
        {
            id: 'e2c565',
            color: 192,
            radius: 1,
            position: { x: -449, y: -209 }
        },
        {
            id: '2884c3',
            color: 24,
            radius: 1,
            position: { x: -697, y: 472 }
        },
        {
            id: '98f777',
            color: 138,
            radius: 1,
            position: { x: 845, y: -690 }
        },
        {
            id: 'da4327',
            color: 52,
            radius: 1,
            position: { x: 480, y: 227 }
        },
        {
            id: '06f275',
            color: 170,
            radius: 1,
            position: { x: 823, y: -156 }
        },
        {
            id: '7b4e81',
            color: 53,
            radius: 1,
            position: { x: 462, y: -886 }
        },
        {
            id: '680fe7',
            color: 77,
            radius: 1,
            position: { x: -424, y: 869 }
        },
        {
            id: 'cf5e87',
            color: 218,
            radius: 1,
            position: { x: 574, y: 38 }
        },
        {
            id: '7e9e5e',
            color: 175,
            radius: 1,
            position: { x: 987, y: 156 }
        },
        {
            id: '8790f1',
            color: 155,
            radius: 1,
            position: { x: 169, y: -690 }
        },
        { id: 'c61f06', color: 8, radius: 1, position: { x: 43, y: 335 } },
        {
            id: '8a4859',
            color: 215,
            radius: 1,
            position: { x: -762, y: 254 }
        },
        {
            id: '47b91b',
            color: 99,
            radius: 1,
            position: { x: -653, y: -20 }
        },
        {
            id: '8b95fc',
            color: 33,
            radius: 1,
            position: { x: -128, y: -127 }
        },
        {
            id: '04cb2f',
            color: 173,
            radius: 1,
            position: { x: -492, y: 125 }
        },
        {
            id: 'de503e',
            color: 103,
            radius: 1,
            position: { x: 115, y: -40 }
        },
        { id: '116186', color: 128, radius: 1, position: { x: 9, y: 138 } },
        {
            id: '5b3fdc',
            color: 248,
            radius: 1,
            position: { x: 918, y: -172 }
        },
        {
            id: '20c2cd',
            color: 107,
            radius: 1,
            position: { x: 204, y: -986 }
        },
        {
            id: '8af91e',
            color: 132,
            radius: 1,
            position: { x: 501, y: 72 }
        },
        {
            id: '4e20a2',
            color: 150,
            radius: 1,
            position: { x: -39, y: 830 }
        },
        {
            id: 'e4209f',
            color: 211,
            radius: 1,
            position: { x: -546, y: -452 }
        },
        {
            id: '556464',
            color: 15,
            radius: 1,
            position: { x: -486, y: 348 }
        },
        {
            id: 'dc6104',
            color: 157,
            radius: 1,
            position: { x: -91, y: 776 }
        },
        {
            id: 'd33802',
            color: 168,
            radius: 1,
            position: { x: 921, y: 77 }
        },
        {
            id: '7bdf1b',
            color: 68,
            radius: 1,
            position: { x: 162, y: 369 }
        },
        {
            id: '629471',
            color: 85,
            radius: 1,
            position: { x: -366, y: 476 }
        },
        {
            id: 'bb12a0',
            color: 115,
            radius: 1,
            position: { x: -124, y: 462 }
        },
    ],
    timestamp: 9052,
    center: { x: 3169, y: 1665 },
    statistics: [{ name: '111', place: 1, me: 1 }]
}

module.exports = {
    initMessage,
    regularMessage,
    regularMessageWithStatistics
}