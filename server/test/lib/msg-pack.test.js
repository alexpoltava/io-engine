const { toBeDeepCloseTo, toMatchCloseTo } = require('jest-matcher-deep-close-to');
const { objectSerialize, objectDeserialize, getMessageDefinition } = require('../../lib/msg-pack');
const {
  DATAFRAME
} = require("../../config");
expect.extend({ toBeDeepCloseTo, toMatchCloseTo });

const { initMessage, regularMessage, regularMessageWithStatistics } = require('./mock-data');

test('should serialize/deserilize init message correctly', () => {
  const message = initMessage;
  const messageDefinition = getMessageDefinition(DATAFRAME.INIT);

  const serialized = objectSerialize(message, messageDefinition);
  const deserialized = objectDeserialize(serialized, messageDefinition);
  expect(deserialized.object).toBeDeepCloseTo(message, 3);
});

test('should serialize/deserilize regular message correctly', () => {
  const message = regularMessage;
  const messageDefinition = getMessageDefinition(DATAFRAME.REGULAR);

  const serialized = objectSerialize(message, messageDefinition);
  const deserialized = objectDeserialize(serialized, messageDefinition);
  expect(deserialized.object).toBeDeepCloseTo(message, 3);
});

test('should serialize/deserilize regular message with statistics correctly', () => {
  const message = regularMessageWithStatistics;
  const messageDefinition = getMessageDefinition(DATAFRAME.REGULAR_WITH_STATISCTICS);

  const serialized = objectSerialize(message, messageDefinition);
  const deserialized = objectDeserialize(serialized, messageDefinition);
  expect(deserialized.object).toBeDeepCloseTo(message, 3);
});
