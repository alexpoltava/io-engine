const {
  distance,
  normalize,
  getShortUid,
  get32BitTimestamp,
} = require('../../lib/util');

const { MAX_FORCE } = require('../../config');

test('should calculate 0 distance correctly', () => {
  expect(distance([0, 0], [0, 0])).toBe(0);
});

test('should calculate positive distance correctly', () => {
  expect(distance([0, 0], [100, 100])).toBeCloseTo(141.421, 3);
});

test('should calculate negative distance correctly', () => {
  expect(distance([0, 0], [-100, -100])).toBeCloseTo(141.421, 3);
});

test('should normalize force correctly', () => {
  expect(normalize([0, 0])).toHaveLength(2);
  expect(normalize([0, 1500], MAX_FORCE)).toStrictEqual([0, MAX_FORCE]);
  expect(normalize([0, -1500], MAX_FORCE)).toStrictEqual([0, -MAX_FORCE]);
  expect(normalize([1500, 0], MAX_FORCE)).toStrictEqual([MAX_FORCE, 0]);
  expect(normalize([-1500, 0], MAX_FORCE)).toStrictEqual([-MAX_FORCE, 0]);
  expect(normalize([1500, 1500], MAX_FORCE)[0]).toBeCloseTo(MAX_FORCE * 0.7071, 1);
  expect(normalize([1500, 1500], MAX_FORCE)[1]).toBeCloseTo(MAX_FORCE * 0.7071, 1);
  expect(normalize([-1500, -1500], MAX_FORCE)[0]).toBeCloseTo(-MAX_FORCE * 0.7071, 1);
  expect(normalize([-1500, -1500], MAX_FORCE)[1]).toBeCloseTo(-MAX_FORCE * 0.7071, 1);
  expect(normalize([-1500, 1500], MAX_FORCE)[0]).toBeCloseTo(-MAX_FORCE * 0.7071, 1);
  expect(normalize([-1500, 1500], MAX_FORCE)[1]).toBeCloseTo(MAX_FORCE * 0.7071, 1);
  expect(normalize([1500, -1500], MAX_FORCE)[0]).toBeCloseTo(MAX_FORCE * 0.7071, 1);
  expect(normalize([1500, -1500], MAX_FORCE)[1]).toBeCloseTo(-MAX_FORCE * 0.7071, 1);
  // expect(normalize([20, -320], MAX_FORCE)[0]).toBeCloseTo(MAX_FORCE * 0.06237, 1);
  // expect(normalize([20, -320], MAX_FORCE)[1]).toBeCloseTo(-MAX_FORCE * 0.99805, 1);
});

test('should short uid correctly', () => {
  expect(getShortUid('5feb31a0-2eac-428d-8e3d-ade152b1544d', 4)).toBe('544d');
  expect(getShortUid('5feb31a0-2eac-428d-8e3d-ade152b1544d', 4, true)).toBe('5feb');
});

test('should return 32BitTimestamp correctly', () => {
  const sampleTimestamp = 1591618394523;
  const sample32BitTimestamp = 0x93d9599b;
  expect(get32BitTimestamp(sampleTimestamp)).toBe(sample32BitTimestamp);
  const start = (Buffer.from(Date.now().toString(16).padStart(12, "0"), 'hex')).readUInt32BE(2);
  const result = get32BitTimestamp();
  const finish = (Buffer.from(Date.now().toString(16).padStart(12, "0"), 'hex')).readUInt32BE(2);
  expect(result).toBeGreaterThanOrEqual(start);
  expect(result).toBeLessThanOrEqual(finish);
})