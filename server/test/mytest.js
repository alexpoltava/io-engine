const { objectDeserialize, objectSerialize, getMessageDefinition } = require('../lib/msg-pack');
const mockMsg = { "players": [{ "id": "105", "color": 150, "radius": 10, "inGame": 0, "name": "", "mass": 0.29389992000000004, "me": 1, "position": { "x": 0, "y": 0 } }], "food": [], "timestamp": 2835173065, "center": { "x": 794, "y": 3246 } };

setTimeout(() => {
    const mockCustomFrame = Buffer.from([0, 36, 154, 37, 44, 0, 1, 16, 220, 0, 10, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 168, 213, 6, 11, 36, 154, 37, 44]).toString('hex');

    const data = Buffer.from(mockCustomFrame, 'hex');
    const messageType = data.readUInt8(0);
    const messageBuffer = data.slice(1);
    objectDeserialize(messageBuffer, getMessageDefinition(messageType)).object;
}, 10000);

// setTimeout(() => {
//     const s = objectSerialize(mockMsg, getMessageDefinition(0));
// }, 20000);


