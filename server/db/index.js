const { db } = require('../service/firebase-admin');

const saveUserGameStatistics = (id, statistics) => {
    const statisticsRef = db.ref(`users/${id}/statistics`);

    return statisticsRef.push().set(statistics);
}

module.exports = {
    saveUserGameStatistics,
}
