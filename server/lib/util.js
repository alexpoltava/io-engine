const util = require("util");
const fs = require('fs');
const zlib = require("zlib");
const { objectSerialize, getMessageDefinition } = require('./msg-pack');

const distance = ([x1, y1], [x2, y2]) => {
  return Math.pow(Math.pow(x1 - x2, 2) + Math.pow(y1 - y2, 2), 1 / 2);
};

const normalize = ([x, y], max) => {
  const parameter = distance([0, 0], [x, y]);

  if (parameter > max) {
    const cosAlpha = x / parameter;
    const sinAlpha = y / parameter;
    return [max * cosAlpha, max * sinAlpha];
  } else {
    return [x, y];
  }
};

const sendCompressed = (client, buffer) => {
  if (client && buffer) {
    zlib.deflate(buffer, (err, compressedBuffer) => {
      if (err) throw err;
      client.send(compressedBuffer);
    });
  } else {
    throw new Error("Missing required data");
  }
};

const composeFrame = (message, messageType) => {
  const serialized = objectSerialize(message, getMessageDefinition(messageType));
  return Buffer.concat([Buffer.from([messageType]), serialized]);
};

const get32BitTimestamp = timestamp => {
  const now = timestamp || Date.now();
  const buff = Buffer.from(now.toString(16).padStart(12, "0"), 'hex');
  return buff.readUInt32BE(2);
}

const isVisible = (body, center, width, height) => {
  const { x, y } = body.position;
  const { circleRadius } = body;
  const cx = center.x;
  const cy = center.y;

  const leftX = cx - width / 2;
  const rightX = cx + width / 2;
  const topY = cy + height / 2;
  const bottomY = cy - height / 2;

  const visible =
    x + circleRadius >= leftX &&
    x - circleRadius <= rightX &&
    y - circleRadius <= topY &&
    y + circleRadius >= bottomY;

  return visible;
}

module.exports = {
  normalize,
  distance,
  sendCompressed,
  composeFrame,
  get32BitTimestamp,
  isVisible,
};
