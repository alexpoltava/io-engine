const WebSocket = require('ws');
const url = require('url');
const { auth } = require('../service/firebase-admin');

class Connection {
  #wss;

  constructor({ server, onConnect, onClose, onMessage }) {
    this.#wss = new WebSocket.Server({ server, verifyClient: this.verifyClient });

    this.initWS(onConnect, onClose, onMessage);
  }

  initWS(onConnect, onClose, onMessage) {
    this.#wss.on('connection', (ws, req) => {
      ws.id = req.uid;
      onConnect(ws);

      ws.on('close', () => {
        onClose(ws.id)
      });

      ws.on('message', (data) => {
        onMessage(ws.id, data);
      })
    });
  }

  async verifyClient({ origin, req, secure }, callback) {
    try {
      const url_parts = url.parse(req.url, true);
      const query = url_parts.query;
      const decodedToken = await auth.verifyIdToken(query.token)
      req.uid = decodedToken.uid;
      callback(true);
    } catch (e) {
      callback(false);
    }
  }

  getClients() {
    return this.#wss.clients;
  }
}

module.exports = Connection;
