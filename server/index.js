const app = require('express')();
const server = require('http').createServer(app);
const port = process.env.PORT || 8000;
const Game = require('./core/game');
const Connection = require('./lib/connection');

const game = new Game({
  Connection,
  server
});

server.listen(port, function () {
  console.log('listening on *:' + port);
});
