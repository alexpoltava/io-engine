const WebSocket = require("ws");

const {
  STATE_UPDATE_INTERVAL,
  SEND_STATE_UPDATE_INTERVAL,
  MAX_FORCE,
  MAX_FOOD_NUMBER,
  MAX_FOOD_PER_ROUND,
  FOOD_ADD_INTERVAL,
  CLIENT_MESSAGES,
  COLORS_NUMBER,
  DATAFRAME,
} = require("../config");
const { GameWorld } = require("../physics");

const {
  composeFrame,
  get32BitTimestamp,
  normalize,
  sendCompressed,
  isVisible,
} = require("../lib/util");
const { saveUserGameStatistics } = require("../db");

class Game {
  #connection;
  #world;

  constructor({ Connection, server }) {
    this.colors = [];
    this.initColors();

    this.getDataForPlayer = this.getViewportData.bind(this);
    this.onConnect = this.onConnect.bind(this);
    this.onClose = this.onClose.bind(this);
    this.onMessage = this.onMessage.bind(this);

    this.removePlayer = this.removePlayer.bind(this);

    this.#connection = new Connection({
      server,
      onConnect: this.onConnect,
      onClose: this.onClose,
      onMessage: this.onMessage,
    });

    this.prevUpdate = get32BitTimestamp();
    this.hrprev = process.hrtime();

    this.#world = new GameWorld({
      onConsumeFood: this.onConsumeFood.bind(this),
      onConsumePlayer: this.onConsumePlayer.bind(this),
    });

    this.runStateUpdateCycle();
    this.runSendStateUpdateCycle();
  }

  initColors() {
    for (let i = 0; i < COLORS_NUMBER; i++) {
      this.colors.push(Math.round(Math.random() * 0xffffff));
    }
  }

  updateState() {
    this.#world.update(process.hrtime(this.hrprev)[1] * 1e-6);
    this.addFoodRound();

    this.hrprev = process.hrtime();
    this.prevUpdate = get32BitTimestamp();
  }

  runStateUpdateCycle() {
    setInterval(() => {
      this.updateState();
    }, STATE_UPDATE_INTERVAL);
  }

  addFoodRound() {
    const currentFoodCount = this.#world.getFoodCount();
    if (currentFoodCount < MAX_FOOD_NUMBER) {
      if (
        !this.lastFoodAdded ||
        Date.now() - this.lastFoodAdded > FOOD_ADD_INTERVAL
      ) {
        this.lastFoodAdded = Date.now();
        const numberToAdd = Math.min(
          MAX_FOOD_PER_ROUND,
          MAX_FOOD_NUMBER - currentFoodCount
        );
        this.#world.addFood(numberToAdd);
        console.log("total food in the world: ", this.#world.getFoodCount());
      }
    }
  }

  onConsumeFood(player, food) {
    if (!this.#world.isPlayerInGame(player)) {
      return;
    }
    this.#world.scalePlayer(
      player,
      Math.pow((player.mass + food.mass) / player.mass, 1 / 2)
    );

    this.#world.updatePlayerGameStatistics(player, {
      foodEaten: this.#world.getPlayerStatistics(player).foodEaten + 1,
    });
    this.#world.removeFromWorld(food);
  }

  onConsumePlayer(player, anotherPlayer) {
    if (
      !this.#world.isPlayerInGame(player) ||
      !this.#world.isPlayerInGame(anotherPlayer)
    ) {
      return;
    }

    this.#world.scalePlayer(
      player,
      Math.pow((player.mass + anotherPlayer.mass) / player.mass, 1 / 2)
    );

    this.#world.updatePlayerGameStatistics(player, {
      playerCellsEaten:
        this.#world.getPlayerStatistics(player).playerCellsEaten + 1,
    });

    this.leaveGame(anotherPlayer);
  }

  getViewportData(currentPlayer) {
    // create player's viewport (move to center, cut unvisible)
    const { position } = currentPlayer;
    const screen = this.#world.getPlayerScreen(currentPlayer);
    const dx = position.x;
    const dy = position.y;
    const center = {
      x: position.x,
      y: position.y,
    };
    const message = {
      center,
      players: this.#world
        .getPlayers()
        .filter(
          (player) =>
            player.id === currentPlayer.id ||
            (screen &&
              isVisible(player, center, screen.width, screen.height) &&
              this.#world.isPlayerInGame(player))
        )
        .map((player) => ({
          id: player.id,
          color: this.#world.getBodyColor(player),
          radius: Math.round(player.circleRadius),
          inGame: +this.#world.isPlayerInGame(player),
          name: this.#world.getPlayerName(player),
          mass: player.mass * 10,
          speed: Math.hypot(player.velocity.x, player.velocity.y) * 10,
          me: +(player.id === currentPlayer.id),
          position: {
            x: player.position.x - dx,
            y: player.position.y - dy,
          },
        })),
      food: this.#world
        .getFood()
        .filter(
          (food) =>
            screen && isVisible(food, center, screen.width, screen.height)
        )
        .map((food) => ({
          id: food.id,
          color: this.#world.getBodyColor(food),
          radius: Math.round(food.circleRadius),
          position: {
            x: food.position.x - dx,
            y: food.position.y - dy,
          },
        })),
      ...(Math.round(Math.random() * 20) === 10 && {
        // update leaderboard ~ once per 20 messages
        statistics: (() => {
          const playersInGame = this.#world
            .getPlayersInGame()
            .sort((a, b) => b.mass - a.mass);
          const me = playersInGame.findIndex(
            (player) => player.id === currentPlayer.id
          );
          const statistics = playersInGame.slice(0, 10).map((player, i) => ({
            name: this.#world.getPlayerName(player),
            place: i + 1,
            mass: player.mass * 10,
            me: +(me === i),
          }));

          return me > 10
            ? [
              ...statistics,
              {
                name: this.#world.getPlayerName(playersInGame[me]),
                place: me + 1,
                mass: player.mass * 10,
                me: 1,
              },
            ]
            : statistics;
        })(),
      }),
      timestamp: this.prevUpdate,
    };

    return message;
  }

  broadcast() {
    const clients = this.#connection.getClients();
    for (const client of clients) {
      if (client.readyState === WebSocket.OPEN) {
        const currentPlayer = this.#world.getPlayerByClientId(client.id);
        if (currentPlayer) {
          const data = this.getViewportData(currentPlayer);
          sendCompressed(
            client,
            composeFrame(
              data,
              data.hasOwnProperty("statistics")
                ? DATAFRAME.REGULAR_WITH_STATISCTICS
                : DATAFRAME.REGULAR
            )
          );
        } else {
          console.log("Client is not added as a player");
        }
      } else {
        this.removePlayer(client);
      }
    }
  }

  runSendStateUpdateCycle() {
    setInterval(() => {
      this.broadcast();
    }, SEND_STATE_UPDATE_INTERVAL);
  }

  sendInitialData(client) {
    const data = {
      colors: this.colors,
    };
    sendCompressed(client, composeFrame(data, DATAFRAME.INIT));
  }

  addPlayer(client) {
    const clientId = client.id;
    if (this.#world.getPlayerByClientId(clientId)) {
      console.log(`Player with id ${clientId} is already registered`);
    } else {
      this.#world.addPlayer(clientId);
      setTimeout(() => this.sendInitialData(client), 200); // time for set listener // TODO: review client composition
    }
  }

  joinGame(player, name) {
    this.#world.joinGame(player, name);
    console.log(`player ${name} joined the game`);
  }

  leaveGame(player) {
    const name = this.#world.getPlayerName(player);
    const clientId = this.#world.getCustomSettings(player).clientId;

    this.#world.leaveGame(player);

    const statistics = this.#world.getPlayerStatistics(player);

    saveUserGameStatistics(clientId, statistics).catch((e) =>
      console.log(e.message)
    );

    console.log(`player ${name} left the game`);
  }

  removePlayer(clientId) {
    const playerToRemove = this.#world.getPlayerByClientId(clientId);

    if (playerToRemove) {
      if (this.#world.isPlayerInGame(playerToRemove)) {
        this.leaveGame(playerToRemove);
      }
      this.#world.removeFromWorld(playerToRemove);
    }
  }

  processMessage(message, player) {
    const command = CLIENT_MESSAGES.find((command) =>
      message.hasOwnProperty(command)
    );
    switch (command) {
      case "force": {
        if (this.#world.isPlayerInGame(player)) {
          const { x, y } = message.force;
          const [normX, normY] = normalize([x, y], MAX_FORCE);
          this.#world.updatePlayerForce(player, normX, normY);
        }
        return;
      }
      case "join": {
        this.joinGame(player, message.join.name);
        return;
      }
      case "screen": {
        const name = this.#world.getPlayerName(player);
        console.log(
          `"screen" message from player ${name}: ${JSON.stringify(
            message.screen
          )}`
        );
        this.#world.updatePlayerScreen(
          player,
          message.screen.width,
          message.screen.height
        );
      }
      default: {
        return;
      }
    }
  }

  onConnect(client) {
    console.log(`registering client ${client.id}`);
    this.addPlayer(client);
  }

  onClose(id) {
    console.log(`unregistering client ${id}`);
    this.removePlayer(id);
  }

  onMessage(clientId, data) {
    const message = JSON.parse(Buffer.from(data).toString());
    const player = this.#world.getPlayerByClientId(clientId);
    if (player) {
      this.processMessage(message, player);
    }
  }
}

module.exports = Game;
