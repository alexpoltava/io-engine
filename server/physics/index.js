const Matter = require("matter-js");
const MatterAttractors = require("matter-attractors-f");
const { Engine, World, Bodies, Body, Events, Vector } = Matter;

const {
  FIELD_SIZE_X,
  FIELD_SIZE_Y,
  MAX_SCREEN_WIDTH,
  MAX_SCREEN_HEIGHT,
  PHYSICS,
  COLORS_NUMBER,
} = require("../config");

// collision filter categories
const CAN_COLLIDE = 1;
const CAN_NOT_COLLIDE = 2;

class GameWorld {
  #engine;

  constructor({ onConsumeFood, onConsumePlayer }) {
    Matter.use(MatterAttractors);

    this.#engine = Engine.create();
    this.#engine.world.gravity.scale = 0;

    this.createWorldBoundaries(FIELD_SIZE_X, FIELD_SIZE_Y);
    this.registerListeners(onConsumeFood, onConsumePlayer);
  }

  registerListeners(onConsumeFood, onConsumePlayer) {
    Matter.after("Engine.update", () => {
      const players = this.getPlayers().filter((player) =>
        this.isPlayerInGame(player)
      );
      for (const player of players) {
        const force = this.getPlayerForce(player);
        Body.applyForce(player, player.position, force);
      }
    });

    Events.on(this.#engine, "collisionStart", ({ pairs }) => {
      for (const pair of pairs) {
        this.processCollision(onConsumeFood, onConsumePlayer, pair);
      }
    });
  }

  processCollision(onConsumeFood, onConsumePlayer, pair) {
    if (
      (pair.bodyA.label !== "boundary" || pair.bodyB.label !== "boundary") &&
      (pair.bodyA.label === "player" || pair.bodyB.label === "player")
    ) {
      if (pair.bodyA.label === "player" && pair.bodyB.label === "player") {
        // player + player
        if (pair.bodyA.mass > pair.bodyB.mass * 1.05) {
          onConsumePlayer(pair.bodyA, pair.bodyB);
        } else if (pair.bodyB.mass > pair.bodyA.mass * 1.05) {
          onConsumePlayer(pair.bodyB, pair.bodyA);
        }
        return;
      }

      if (pair.bodyA.label === "food") {
        // food + player
        onConsumeFood(pair.bodyB, pair.bodyA);
      }
      if (pair.bodyB.label === "food") {
        // player + food
        onConsumeFood(pair.bodyA, pair.bodyB);
      }
    }
  }

  addToWorld(...args) {
    World.add(this.#engine.world, ...args);
  }

  removeFromWorld(body) {
    World.remove(this.#engine.world, body);
  }

  createWorldBoundaries(width, height) {
    const options = {
      isStatic: true,
      label: "boundary",
      collisionFilter: {
        category: CAN_COLLIDE,
        mask: CAN_COLLIDE,
      },
    };

    const BOUNDARY_THICKNESS = 500;

    const left = Bodies.rectangle(
      0 - BOUNDARY_THICKNESS / 2,
      0 + height / 2,
      BOUNDARY_THICKNESS,
      height + BOUNDARY_THICKNESS,
      options
    );
    const right = Bodies.rectangle(
      width + BOUNDARY_THICKNESS / 2,
      0 + height / 2,
      BOUNDARY_THICKNESS,
      height + BOUNDARY_THICKNESS,
      options
    );
    const bottom = Bodies.rectangle(
      0 + width / 2,
      0 - BOUNDARY_THICKNESS / 2,
      width + BOUNDARY_THICKNESS,
      BOUNDARY_THICKNESS,
      options
    );
    const top = Bodies.rectangle(
      0 + width / 2,
      height + BOUNDARY_THICKNESS / 2,
      width + BOUNDARY_THICKNESS,
      BOUNDARY_THICKNESS,
      options
    );
    this.addToWorld([left, right, bottom, top]);
  }

  addPlayer(clientId) {
    const options = {
      isStatic: false,
      label: "player",
      density: PHYSICS.DENSITY,
      frictionAir: PHYSICS.FRICTION_AIR,
      collisionFilter: {
        category: CAN_NOT_COLLIDE,
        mask: CAN_NOT_COLLIDE,
      },
      plugin: {
        attractors: [
          (bodyA, bodyB) => {
            if (bodyB.label === "food") {
              const distance = Math.hypot(
                bodyA.position.x - bodyB.position.x,
                bodyA.position.y - bodyB.position.y
              );
              if (distance < 2 * bodyA.circleRadius) {
                return {
                  x: (bodyA.position.x - bodyB.position.x) * 1e-4,
                  y: (bodyA.position.y - bodyB.position.y) * 1e-4,
                };
              }
            }
          },
        ],
        customSettings: {
          clientId,
          name: "",
          color: Math.round(Math.random() * COLORS_NUMBER),
          screen: { width: MAX_SCREEN_WIDTH, height: MAX_SCREEN_HEIGHT },
          force: { x: 0, y: 0 },
        },
      },
    };

    const x = Math.random() * FIELD_SIZE_X;
    const y = Math.random() * FIELD_SIZE_Y;
    const player = Bodies.circle(x, y, PHYSICS.PLAYER_INITIAL_RADIUS, options);
    this.addToWorld([player]);
  }

  updateCustomSettings(body, params) {
    Body.set(body, {
      plugin: {
        ...body.plugin,
        customSettings: {
          ...body.plugin.customSettings,
          ...params,
        },
      },
    });
  }

  updatePlayerForce(player, x, y) {
    this.updateCustomSettings(player, {
      force: {
        x: x * 1e-3,
        y: y * 1e-3,
      },
    });
  }

  getCustomSettings(body) {
    return body.plugin.customSettings || {};
  }

  updatePlayerGameStatistics(player, params) {
    const currentStatistics = this.getPlayerStatistics(player);
    this.updateCustomSettings(player, {
      statistics: {
        ...currentStatistics,
        ...params,
      },
    });
  }

  resetPlayerGameStatistics(player) {
    const initStatistics = {
      highestMass: 0,
      foodEaten: 0,
      playerCellsEaten: 0,
      topPosition: null,
      leaderboardTime: 0,
      gameStarted: null,
      gameEnded: null,
    };
    this.updatePlayerGameStatistics(player, initStatistics);
  }

  scalePlayer(player, scale = 1) {
    Body.scale(player, scale, scale);
  }

  updatePlayerPosition(player, x, y) {
    Body.setPosition(player, { x, y });
  }

  updatePlayerName(player, name) {
    this.updateCustomSettings(player, { name });
  }

  updatePlayerInGameState(player, inGame) {
    this.updateCustomSettings(player, { inGame });
  }

  updatePlayerScreen(player, width, height) {
    this.updateCustomSettings(player, {
      screen: {
        width,
        height,
      },
    });
  }

  updatePlayerCollisionFilter(player, category, mask) {
    player.collisionFilter = { ...player.collisionFilter, category, mask };
  }

  joinGame(player, name) {
    this.resetPlayerGameStatistics(player);
    this.scalePlayer(
      player,
      PHYSICS.PLAYER_INITIAL_RADIUS / player.circleRadius
    );
    this.updatePlayerPosition(
      player,
      Math.random() * FIELD_SIZE_X,
      Math.random() * FIELD_SIZE_Y
    );
    this.updatePlayerName(player, name);
    this.updatePlayerGameStatistics(player, {
      gameStarted: Date.now(),
      highestMass: player.mass,
    });
    this.updatePlayerInGameState(player, true);
    this.updatePlayerCollisionFilter(player, CAN_COLLIDE, CAN_COLLIDE);
  }

  leaveGame(player) {
    this.updatePlayerInGameState(player, false);
    this.updatePlayerCollisionFilter(player, CAN_NOT_COLLIDE, CAN_NOT_COLLIDE);
    this.updatePlayerGameStatistics(player, {
      gameEnded: Date.now(),
    });
  }

  getPlayerScreen(player) {
    return this.getCustomSettings(player).screen;
  }

  getPlayerStatistics(player) {
    return this.getCustomSettings(player).statistics || {};
  }

  getPlayerName(player) {
    return this.getCustomSettings(player).name || "";
  }

  getPlayerForce(player) {
    return this.getCustomSettings(player).force || {};
  }

  isPlayerInGame(player) {
    return Boolean(this.getCustomSettings(player).inGame);
  }

  addFood(numberToAdd = 1) {
    for (let i = 0; i < numberToAdd; i++) {
      const x =
        PHYSICS.FOOD_RADIUS +
        Math.random() * (FIELD_SIZE_X - 2 * PHYSICS.FOOD_RADIUS);
      const y =
        PHYSICS.FOOD_RADIUS +
        Math.random() * (FIELD_SIZE_Y - 2 * PHYSICS.FOOD_RADIUS);

      const options = {
        isStatic: false,
        isSensor: true,
        label: "food",
        density: PHYSICS.FOOD_DENSITY,
        plugin: {
          customSettings: {
            color: Math.round(Math.random() * COLORS_NUMBER),
          },
        },
        collisionFilter: {
          category: CAN_COLLIDE,
          mask: CAN_COLLIDE,
        },
      };

      const food = Bodies.circle(x, y, PHYSICS.FOOD_RADIUS, options);
      this.addToWorld([food]);
    }
  }

  getBodyColor(body) {
    return this.getCustomSettings(body).color || 0;
  }

  getPlayerByClientId(clientId) {
    return this.getPlayers().find(
      (player) => this.getCustomSettings(player).clientId === clientId
    );
  }

  getPlayers() {
    return this.#engine.world.bodies.filter((body) => body.label === "player");
  }

  getPlayersInGame() {
    return this.getPlayers().filter((player) => this.isPlayerInGame(player));
  }

  getPlayersCount() {
    return this.getPlayers().length;
  }

  getFood() {
    return this.#engine.world.bodies.filter((body) => body.label === "food");
  }

  getFoodCount() {
    return this.getFood().length;
  }

  getWorldState() {
    return this.#engine.world;
  }

  update(interval) {
    Engine.update(this.#engine, interval);
  }
}

module.exports = {
  GameWorld,
};
